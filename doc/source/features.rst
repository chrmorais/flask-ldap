********
Features
********

* support of openLDAP and Active Directory
* friendly API
* perfect integration with Flask
* save profile into MongoDB (Flask-PyMongo required)